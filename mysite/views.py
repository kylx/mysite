from django.http import Http404, HttpResponse, JsonResponse
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout    
from django.contrib.auth.models import User
from django.views.generic import View
from django.core.exceptions import ObjectDoesNotExist

import datetime

from .forms import UserForm



def login(request):
    error = None
    if request.method == "POST":
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                auth_login(request, user)
                return redirect(request.GET.get('next', 'list-books'))
            else:
                error = 'User is not active!'
        else:
            error = f'Invalid username or password!'
    
    context = {
        'error': error,
    }
    return render(request, 'login.html', context)

def register(request):
    if request.method == "POST":
        form = UserForm(request.POST)
        # print(f'{form}')
        if form.is_valid():
            user = form.save()
            print(f'{user}')
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=raw_password)
            print(f'{username}-{raw_password}')
            if user:
                auth_login(request, user)
                return redirect('list-books')
    else:
        form = UserForm()
    
    context = {
        'form': form,
    }
    return render(request, 'register.html', context)

def logout(request):
    auth_logout(request)
    return redirect('list-books')



def hello(request):
    context = {
        'page_name': 'hello',
    }
    return render(request, 'mysite/hello.html', context)


def current_datetime(request):
    date = datetime.datetime.now()
    context = {
        'page_name': 'date',
        'date': date,
    }
    return render(request, 'mysite/date.html', context)

def hours_ahead(request, offset):
    try:
        offset = int(offset)
    except ValueError:
        raise Http404()
    date = datetime.datetime.now() + datetime.timedelta(hours=offset)
    context = {
        'page_name': 'hours_ahead',
        'hours_ahead': offset,
        'date': date,
    }
    return render(request, 'mysite/hours_ahead.html', context)

def math(request, a, b, c):
    try:
        a = int(a)
        b = int(b)
        c = int(c)
    except ValueError:
        raise Http404()

    sum = a + b + c
    diff = a - b - c
    prod = a * b * c
    quot = None
    divide_by_zero = False
    try:
        quot = a / b / c
    except ZeroDivisionError:
        divide_by_zero = True

    
    context = {
        'page_name': 'math',
        'a': a,
        'b': b,
        'c': c,
        'sum': sum,
        'diff': diff,
        'prod': prod,
        'quot': quot,
        'divide_by_zero': divide_by_zero,
    }
    return render(request, 'mysite/math.html', context)

def valid_date(request, year, month, day):
    valid = True
    date = None

    try:
        date = datetime.datetime(year=int(year), month=int(month), day=int(day))
    except ValueError:
        valid = False

    context = {
        'page_name': 'valid_date',
        'year': year,
        'month': month,
        'day': day,
        'date': date,
        'valid': valid,
    }
    return render(request, 'mysite/valid_date.html', context)



class UserSearch(View):
    def get(self, request):
        username = request.GET.get('username')
        print(f'user: {username}')
        try:
            User.objects.get(username=username)
            data = {'user_exists': True}
        except ObjectDoesNotExist:
            data = {'user_exists': False}
        return JsonResponse(data)