from django.contrib import admin

from books.models import *


class AuthorAdmin(admin.ModelAdmin):
    fields = ['email', 'first_name', 'last_name']
    search_fields = ['first_name', 'last_name']


class BookAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {'fields': ['title', 'authors', 'publisher']}),
        ('Date information', {
            'fields': ['publication_date'],
            'classes': ['collapse'],
        }),
        (None, {'fields': ['classification']}),
    ]
    list_display = ['title', 'publisher', 'was_published_recently']
    list_filter = ['publication_date']
    search_fields = ['title']
    filter_horizontal = ['authors']

class BookInline(admin.TabularInline):
    model = Book
    extra = 3
    
class PublisherAdmin(admin.ModelAdmin):
    inlines = [BookInline]
    search_fields = ['name', 'city', 'country', 'website']
    list_display = ['name', 'city', 'country', 'website']

admin.site.register(Author, AuthorAdmin)
admin.site.register(Book, BookAdmin)
admin.site.register(Publisher, PublisherAdmin)
admin.site.register(Classification)
