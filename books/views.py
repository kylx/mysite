from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.http import Http404
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic import View
from django.http import JsonResponse
from django.core import serializers

from .forms import basic_model_form
from .models import *

class PublishersListView(ListView):
    model = Publisher
    template_name = 'books/publishers.html'
    context_object_name = 'publishers'

    def get_queryset(self):
        name = self.request.GET.get('name')
        if name:
            self.extra_context = {
                'page_name': 'Publishers search by name',
                'name': name,
            }
            return super().get_queryset().filter(name__icontains=name)
        else:
            self.extra_context = {
                'page_name': 'Publishers',
            }
            return super().get_queryset()
    
    def get_context_data(self, **kwargs):
        name = self.request.GET.get('name')
        if name:
            self.extra_context = {
                'page_name': 'Publishers search by name',
                'name': name,
            }
        else:
            self.extra_context = {
                'page_name': 'Publishers',
            }
        return super().get_context_data(**kwargs)

class AuthorsListView(ListView):
    model = Author
    template_name = 'books/authors.html'
    context_object_name = 'authors'

    def get_queryset(self):
        name = self.__get_name()
        if name:
            return super().get_queryset().filter(Q(first_name__icontains=name) | Q(last_name__icontains=name))
        else:
            return super().get_queryset()

    def get_context_data(self, **kwargs):
        if self.__get_name():
            self.extra_context = {
                'page_name': 'Authors search by name',
                'name': self.__get_name(),
            }
        else:
            self.extra_context = {
                'page_name': 'Authors'
            }
        return super().get_context_data(**kwargs)
    
    def __get_name(self):
        return self.request.GET.get('name')


class ClassificationListView(ListView):
    model = Classification
    template_name = 'books/classifications.html'
    context_object_name = 'classifications'
    extra_context = {
        'page_name': 'Classifications'
    }
    

class BookListView(ListView):
    model = Book
    template_name = 'books/books.html'
    context_object_name = 'books'

    def get_queryset(self):
        author = self.__get_author()
        classification = self.__get_classification()
        if author:
            return super().get_queryset().filter(authors__in=[author])
        elif classification:
            return super().get_queryset().filter(classification=classification)
        else:
            return super().get_queryset()

    def get_context_data(self, **kwargs):
        author = self.__get_author()
        classification = self.__get_classification()
        if author:
            self.extra_context = {
                'page_name': f"Books by {author}",
                'author': author.pk,
            }
        elif classification:
            self.extra_context = {
                'page_name': f"Books classified as {classification.code}",
                'classification': classification.pk,
            }
        return super().get_context_data(**kwargs)

    def __get_author(self):
        author_id = self.kwargs.get('author_id')
        if author_id is None:
            return None
        return get_object_or_404(Author, id=author_id)

    def __get_classification(self):
        classification_id = self.kwargs.get('classification_id')
        if classification_id is None:
            return None
        return get_object_or_404(Classification, id=classification_id)


class BookSearch(ListView):
    model = Book

    def get_queryset(self):
        title = self.request.GET.get('title', None)
        author = self.request.GET.get('author', None)
        classification = self.request.GET.get('classification', None)

        filter_kwargs = {}
        if title:
            filter_kwargs['title__icontains']=title
        if author:
            filter_kwargs['authors__in']=[int(author)]
        if classification:
            filter_kwargs['classification__pk']=int(classification)
        
        return super().get_queryset().filter(**filter_kwargs)
    
    def render_to_response(self, context, **response_kwargs):
        return JsonResponse(
            serializers.serialize(
                'json', 
                self.get_queryset(), 
                use_natural_foreign_keys=True,
            ),
            safe=False,
        )


class BookDetailView(DetailView):
    model = Book
    template_name = 'books/book.html'
    context_object_name = 'book'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update({
            'page_name': context['object'].title,
        })
        return context


def class_add_view(model_class):
    @method_decorator(login_required, name='dispatch')
    class ModelCreateView(CreateView):
        template_name = 'books/model_create.html'
        success_url = reverse_lazy(f'list-{model_class.__name__.lower()}s')
        form_class = basic_model_form(model_class)
        extra_context = {
            'page_name': f'Create new {model_class.__name__}',
        }
    return ModelCreateView
