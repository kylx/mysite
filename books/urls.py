from django.urls import path

from .views import *
from .forms import *
from .models import *

urlpatterns = [
    # list urls
    path('', BookListView.as_view(), name="list-books"),
    path('classifications/', ClassificationListView.as_view(model=Classification), name="list-classifications"),
    path('authors/', AuthorsListView.as_view(), name='list-authors'),
    path('publishers/', PublishersListView.as_view(), name='list-publishers'),


    # path('search/', BookListView.as_view(), name="books-search"),
    path('authors/<int:author_id>', BookListView.as_view(), name="books/authors"),
    path('classifications/<int:classification_id>', BookListView.as_view(), name="books/classifications"),

    path('<slug:pk>', BookDetailView.as_view(), name="books"),


    path('authors/add/', class_add_view(Author).as_view(), name='authors-add'),
    path('publishers/add/', class_add_view(Publisher).as_view(), name='publishers-add'),
    path('add/', class_add_view(Book).as_view(), name='books-add'),

    path('search/', BookSearch.as_view(), name='books-search'),
]