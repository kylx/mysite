import datetime

from django.db import models
from django.utils import timezone

# Create your models here.

class Publisher(models.Model):
    name = models.CharField(max_length=30)
    address = models.CharField(max_length=50)
    city = models.CharField(max_length=60)
    state_province = models.CharField(max_length=30)
    country = models.CharField(max_length=50)
    website = models.URLField()

    def __str__(self):
        return f'{self.name} -- {self.address}, {self.city}, {self.state_province}, {self.country} -- {self.website}'

    def natural_key(self):
        return self.name

class Author(models.Model):
    first_name = models.CharField(max_length=30)
    last_name = models.CharField(max_length=40)
    email = models.EmailField(verbose_name='e-mail')

    def __str__(self):
        return f'{self.first_name} {self.last_name}'

    def natural_key(self):
        return f'{self.first_name} {self.last_name}'

class Classification(models.Model):
    code = models.CharField(max_length=3)
    name = models.CharField(max_length=100)
    description = models.TextField()
    def __str__(self):
        return f'{self.code} - {self.name} > {self.description}'

    def natural_key(self):
        return self.code

class Book(models.Model):
    title = models.CharField(max_length=100)
    authors = models.ManyToManyField(Author)
    publisher = models.ForeignKey(Publisher, on_delete=models.CASCADE)
    classification = models.ForeignKey(Classification, on_delete=models.CASCADE)
    publication_date = models.DateField()


    def __str__(self):
        return f'{self.title} by {self.authors}. Published by {self.publisher} on {self.publication_date}. Classified as "{self.classification}"'
        
    def was_published_recently(self):
        # convert everything to date objects
        date_today = timezone.now().date()
        date_yesterday = (timezone.now() - datetime.timedelta(days=1)).date()
        publication_date = self.publication_date

        return date_yesterday <= publication_date <= date_today
        # return True

    was_published_recently.admin_order_field = 'publication_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'





