import datetime

from django.test import TestCase, Client
from django.utils import timezone
from django.shortcuts import reverse
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.http import HttpResponse

from .models import *
from .views import *
# Create your tests here.

class BookMethodTests(TestCase):
    def test_was_published_recently(self):
        future_date = timezone.now() + datetime.timedelta(days=1)
        future_book = Book(publication_date=future_date)
        self.assertEqual(future_book.was_published_recently(), False)

        date = timezone.now() + datetime.timedelta(days=0)
        book = Book(publication_date=date)
        self.assertEqual(book.was_published_recently(), True)

        date = timezone.now() + datetime.timedelta(days=-1)
        book = Book(publication_date=date)
        self.assertEqual(book.was_published_recently(), True)

        date = timezone.now() - datetime.timedelta(days=-2)
        book = Book(publication_date=date)
        self.assertEqual(book.was_published_recently(), False)

class BooksViewTests(TestCase):
    def setUp(self):
        self.publisher = Publisher.objects.create(
            name='pn',
            address='pa',
            city='pc',
            state_province='ps',
            country='pc',
            website='pw',
        )
        self.publisher = Publisher.objects.create(
            name='pn2',
            address='pa2',
            city='pc2',
            state_province='ps2',
            country='pc2',
            website='pw2',
        )
        self.author = Author.objects.create(
            first_name='afn',
            last_name='aln',
            email='afn@aln.com'
        )
        self.author2 = Author.objects.create(
            first_name='afn2',
            last_name='aln2',
            email='afn2@aln2.com'
        )
        self.classification1 = Classification.objects.create(
            code='cc1',
            description='cd1'
        )
        self.classification2 = Classification.objects.create(
            code='cc2',
            description='cd2'
        )
        new_book = Book.objects.create(
            title='bt',
            publisher=self.publisher,
            classification=self.classification1,
            publication_date=timezone.now().date()
        )
        new_book.authors.add(self.author)
        self.new_book = new_book
        old_book = Book.objects.create(
            title='bt2',
            publisher=self.publisher,
            classification=self.classification2,
            publication_date=timezone.now().date()
        )
        old_book.authors.add(self.author2)
        self.old_book = old_book


    def test_with_no_filter(self):
        """
        Passing no filter should return all books
        """
        response = self.client.get(reverse('list-books'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            list(response.context['books']),
            [repr(b) for b in Book.objects.all()]
        )

    def test_with_no_books(self):
        """
        Display correct output if database doesn't have books
        """
        Book.objects.all().delete()
        response = self.client.get(reverse('list-books'))
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['books'],
            []
        )
        self.new_book.save()

    def test_search_book_by_title(self):
        """
        Searching for book title '2' should return only one book.
        """
        response = self.client.get(reverse('books-search')+'?title=2')
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(
            response.context['books'],
            [repr(b) for b in [self.old_book]]
        )
    
    def test_search_book_by_author(self):
        """
        Searching for books by author2 should return only old_book.
        """
        response = self.client.get(reverse('books/authors', kwargs={'author_id':self.author2.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['books']), 1)
        self.assertEqual(
            response.context['books'].first().id,
            self.old_book.id
        )

    def test_search_book_by_classification(self):
        """
        Searching for books by clasification1 should return only new_book.
        """
        response = self.client.get(reverse('books/classifications', kwargs={'classification_id':self.classification1.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['books']), 1)
        self.assertEqual(
            response.context['books'].first().id,
            self.new_book.id
        )

    def test_detailed_book_page(self):
        """
        Detailed page should show the correct page
        """
        response = self.client.get(reverse('books', kwargs={'pk': self.old_book.id}))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            response.context['book'].id,
            self.old_book.id
        )

    def test_add_author(self):
        """
        Adding an author should be saved in the database and redirect back to authors list page
        """

        self.username = 'qwe'
        self.password = 'asd'
        self.user = User.objects.create_superuser(
            username=self.username,
            password=self.password,
            email='qwe@asd.com'
        )
        self.client.login(username=self.username, password=self.password)
        response = self.client.post(
            reverse('authors-add'), {
                'first_name': 'xx',
                'last_name': 'yy',
                'email': 'zz@zz.com',
            })
        self.assertEqual(response.status_code, 302)
        self.assertIsNotNone(Author.objects.get(first_name='xx', last_name='yy', email='zz@zz.com'))


        User.objects.all().delete()

class AuthenticationTest(TestCase):
    def setUp(self):
        self.username = 'qwe'
        self.password = 'asd'
        self.user = User.objects.create_superuser(
            username=self.username,
            password=self.password,
            email='qwe@asd.com'
        )

    def test_login_wrong(self):
        """
        Wrong username/password should go back to login page
        """

        users = (
            (self.username, 'x'),
            ('x', self.password),
            ('x', 'y',),
        )
        for user in users:
            response = self.client.post(reverse('login'), {'username':user[0], 'password': user[1]}, follow=True)
            # self.assertRedirects(response, reverse('login'), 200)
            self.assertIsInstance(response, HttpResponse) 

    def test_login_right(self):
        """
        Right username/password should redirect to books list page
        """
        response = self.client.post(
            reverse('login'), {
                'username': self.username, 
                'password': self.password
            }, follow=True)
        # print(response.context['error'])
        self.assertRedirects(response, reverse('list-books'))
        # self.assertIsInstance(response, HttpResponse) 

    def test_logout(self):
        response = self.client.get(reverse('logout'), follow=True)
        self.assertRedirects(response, reverse('list-books'))

