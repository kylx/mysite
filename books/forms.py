from django.forms import ModelForm

def basic_model_form(class_name):
    class BasicForm(ModelForm):
        class Meta:
            model = class_name
            exclude = []
    return BasicForm